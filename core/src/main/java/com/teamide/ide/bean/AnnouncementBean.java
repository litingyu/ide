package com.teamide.ide.bean;

import javax.persistence.Column;
import javax.persistence.Table;

import com.teamide.ide.constant.TableInfoConstant;

@Table(name = TableInfoConstant.ANNOUNCEMENT_INFO)
public class AnnouncementBean extends BaseBean {

	@Column(name = "title", length = 500)
	private String title;

	@Column(name = "type", length = 10)
	private String type;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "top", length = 1)
	private Boolean top;

	@Column(name = "detail", length = 5000)
	private String detail;

	public Boolean getTop() {
		return top;
	}

	public void setTop(Boolean top) {
		this.top = top;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
